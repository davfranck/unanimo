This version implements an other input format. The players enter the words in a google forms, and the data are retrieved via an excel sheet. It's quicker.

## how to

* create a google form with the 8 questions
* read the response with the spreadsheet and export it as a csv file
* paste the data in the `input.csv` file (remove the header first)

## Usage
* run the script with argument 0 (Monday), 1(Tuesday), 2(Wednesday), 3 (Thursday), 4 (Friday) :
* example : `groovy inputFormater.groovy 0` for monday results.
* Check the content of `input.json`.
* run the `unanimo.groovy` file `groovy unanimo.groovy`and voila

## input.csv structure

    date,mail,mot1,mot2,...,mot8
    
Exemple de date : "2017/09/15 2:15:25 PM UTC+2"

## input.json structure

Structure vide : 

    {
        "days": [
        ]
    }

Structure avec data : 

    {
        "days": [
            {
                "players": [
                    {
                        "proposal": [
                            "VACANCES",
                            "ECOLE",
                            "SCOLAIRE",
                            "FOURNITURE",
                            "BOUCHON",
                            "CLASSE",
                            "DUR",
                            "GREVE"
                        ],
                        "name": "xxx@xxx.com"
                    },
                    {
                        "proposal": [
                            "ECOLE",
                            "SCOLAIRE",
                            "CARTABLE",
                            "MAITRESSE",
                            "CLASSE",
                            "ELEVE",
                            "DEPRIME",
                            "JOIE"
                        ],
                        "name": "zzz@zzz.com"
                    }
                ],
                "name": "Lundi",
                "index": 0
            }
        ]
    }
