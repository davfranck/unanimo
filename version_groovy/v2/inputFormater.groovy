import groovy.json.*
import groovy.transform.ToString
import java.text.Normalizer

assert args.length > 0, "Merci de transmettre le jour courant (0,1,2,3,4)"

def jour = args[0] as Integer
assert jour > -1 &&  jour < 5, "Jour de la semaine invalide. Merci de transmettre le jour courant (0,1,2,3,4)"

def allPlayersWordsForADay = new File('input.csv').getText("UTF-8")

def JOURS = [
	"0" : "Lundi",
	"1" : "Mardi",
	"2" : "Mercredi",
	"3" : "Jeudi",
	"4" : "Vendredi"
]

@ToString
class Joueur{
  String name
  List<String> proposal = []
}

@ToString
class Jour {
  int index
  String name
  List<Joueur> players = []
}

@ToString
class Resultats {
  List<Jour> days = []
}

def res = new JsonSlurper().parse(new FileReader("input.json")) as Resultats

Jour jourCourant = new Jour()
jourCourant.index = args[0] as Integer
jourCourant.name = JOURS[args[0] as String]
 
allPlayersWordsForADay.eachLine { line ->
	def tokens = line.tokenize(';')
	
	Joueur joueur = new Joueur(name: (tokens[1].substring(0, tokens[1].length()) ))
	(2..9).each {
		// println "traitement de l'index $it de ${joueur.name}"
		joueur.proposal << formatWord((tokens[it]).substring(0, tokens[it].length()))
	}
	jourCourant.players.add(joueur)
}

res.days.removeAll {it.index == jourCourant.index}
res.days.add(jourCourant)

def json = new JsonBuilder(res).toPrettyString()
json = StringEscapeUtils.unescapeJavaScript(json)
new File("input.json").write(json)

String formatWord(String word) {
	word = Normalizer.normalize(word.toUpperCase(), Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "")
	word.trim().replaceAll('-', ' ')
}