import groovy.json.JsonSlurper
import groovy.transform.ToString
import java.text.Normalizer

// Const
String EGALITE = "ex-aequo"

def input = new File ('.', 'input.json')
def jsonSlurper = new JsonSlurper()
def inputData = jsonSlurper.parseText(input.text)

//println inputData

def players = []
def wordsPerDay = [:]
Map<Word, ArrayList> wordCombinations = [:]

def lastDay = inputData.days.max { it.index }
println "Nb de jours à traiter : ${lastDay.index}"

inputData.days.each { day ->
	println "on traite le jour $day.index"

	Set<Word> allWordsOfADay = []

	day.players.each { inputPlayer ->
		def currentPlayer = players.find { it.name == formatName(inputPlayer.name) }
		if(!currentPlayer) {
			//println "creation du joueur $inputPlayer.name"
			currentPlayer = new Player(name: formatName(inputPlayer.name))
			players << currentPlayer
		}
		//println "on traite le joueur $inputPlayer.name / propositions : $inputPlayer.proposals"
		def words = inputPlayer.proposal.collect { word ->
			word = formatWord(word)
			def currentWord = allWordsOfADay.find { it.value == word }
			if(!currentWord){
				//println "ajout du mot $word"
				currentWord = new Word(value: formatWord(word)) 
				allWordsOfADay << currentWord
			} else {
				//println "le mot $word existe déjà, on incrémente les points"
				currentWord.points = currentWord.points == 0 ? 2 : currentWord.points + 1
			}
            // on ne prend que les mots du dernier jour
			if (wordCombinations.containsKey(currentWord) && day.index == lastDay.index){
				wordCombinations.get(currentWord).add(formatName(inputPlayer.name))
			} else if (day.index == lastDay.index){
				wordCombinations.put(currentWord, [formatName(inputPlayer.name)])
			}
			currentWord
		}
		currentPlayer.proposals << new Proposal(day: day.index, words:words, player: currentPlayer)
	}
	wordsPerDay.put(day.index, allWordsOfADay)
}

//On trie les mots par points (décroissant) avec la liste des joueurs pour chaque mot
wordCombinations = wordCombinations.sort{a,b -> b.getKey().points <=> a.getKey().points}

players.each { player ->
	player.proposals.each {
		it.check()
	}
}

// Tris
Closure totalSort = { a,b -> 
    def res = b.total <=> a.total 
    if(res == 0){
        // en cas d'égalité, on départage en fonction de celui qui a le + scoré sur un jour
        def listA = a.proposals.collect()
        def listB = b.proposals.collect()
        while(res == 0 && !listB.empty && !listA.empty){
            def aMax = listA*.points.max()
            def bMax = listB*.points.max()
            res = bMax <=> aMax
            if(res == 0){
                // next !
                listA.removeAll {it.points == aMax}
                listB.removeAll {it.points == bMax}
            }
        }
        if(res == 0){
            a.infos = EGALITE
            b.infos = EGALITE
        }
    }
    return res
}

Closure daySort = { a,b -> 
    def res = b.proposals.find{it.day == lastDay.index}?.points <=> a.proposals.find{it.day == lastDay.index}?.points
    if(res == 0){
        a.dayInfos = EGALITE
        b.dayInfos = EGALITE
    }
    return res
}

// affichage
def outTemplateText = new File('.', 'resultats.template').text
def engine = new groovy.text.SimpleTemplateEngine()
def template = engine.createTemplate (outTemplateText).make(["players": players, "wordsPerDay":wordsPerDay, "maxDay" : lastDay.index, "totalSort" : totalSort, "daySort" : daySort, "wordsCombinations": wordCombinations])
def inlinedTemplate = CssInliner.inline(template.toString())
def outputDir = "${System.getProperty('user.home')}/Downloads/"
def output = new File(outputDir, 'resultats_v2.html')
output.newWriter().withWriter { it << inlinedTemplate }


@ToString
class Player {
	String name
    String infos
    String dayInfos
	List<Proposal> proposals = []

	int getTotal() { proposals.inject(0){ acc, proposal -> acc + proposal.getPoints() } }
    
    String getAugmentedName(){
        return (infos) ? "${name} (${infos})" : name
    }
    
    String getDayAugmentedName(){
        return (dayInfos) ? "${name} (${dayInfos})" : name
    }


}

@ToString
class Proposal {
	int day
	List<Word> words
	Player player

	int getPoints() { words.inject(0) { acc, word -> acc + word.points } }

	boolean check() {
		if(words.size() != 8) {
			println "nombre de mots invalides pour la proposition du jour $day du joueur $player.name"
		}
		if(words.size() != words.toSet().size()){
			println "présence de doublons dans la proposition du jour $day du joueur $player.name"
		}
	}
}

@ToString
class Word {
	String value
	int points = 0

	boolean equals(Object o){
		return o.value == this.value
	}

	int hashCode(){
		return value.hashCode()
	}
}

// @deprecated : à dégager dès lundi
String formatWord(String word) {
	word = Normalizer.normalize(word.toUpperCase(), Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "")
	word.trim().replaceAll('-', ' ')
}

String formatName(String name){
	return name.toUpperCase()
}